O Terraform trabalha com uma linguagem de configuração construida em blocos como os blocos [provider](https://www.terraform.io/docs/language/providers/index.html), [resource](https://www.terraform.io/docs/language/resources/index.html), [variable](https://www.terraform.io/docs/language/values/index.html) entre outros.  

O objetivo principal da linguagem Terraform é declarar recursos , que representam objetos de infraestrutura. Todos os outros recursos de linguagem existem apenas para tornar a definição de recursos mais flexível e conveniente.

Os blocos são contêineres para outro conteúdo e geralmente representam a configuração de algum tipo de objeto, como um recurso. Os blocos têm um tipo de bloco, podem ter zero ou mais rótulos e ter um corpo que contém qualquer número de argumentos e blocos aninhados. A maioria dos recursos do Terraform é controlada por blocos de nível superior em um arquivo de configuração. Em outras palavras o Terraform trabalha com encapsulamento e instancias de objetos.

Beleza, nada melhor do que um Hello World para começar uma nova linguagem né?

No terraform o resultado de uma execução e tratado como uma saida da propria execução.

Para definir uma saída, você abre um bloco de saída usando a palavra chave *output*. Você então começa o bloco *output* com {. Você só tem permissão para definir uma única propriedade chamada value. Qualquer valor que você der à propriedade value será enviado ao console após um sucesso terraform apply. Você então fecha o bloco de saída com }.

```terraform
output "message" {
    value = "Hello World"
}
```
A saida será essa: 

![output exemplo](https://dev-to-uploads.s3.amazonaws.com/uploads/articles/aiavl59mrjtqzo0sp09j.png)

Para saber mais sobre a linguagem e só clicar [aqui](https://www.terraform.io/docs/language/index.html)

Vlw Flw