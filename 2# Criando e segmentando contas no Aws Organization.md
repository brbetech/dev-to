![tumbnail](https://dev-to-uploads.s3.amazonaws.com/uploads/articles/0ua7tvo0l1it4up5q10a.png)

# Criando e segmentando contas no Aws Organization
### #aws #terraform #cloudnative #devops

Vamos conversar um pouco sobre algumas boas práticas.

Em **DDD**, o design da solução é orientado pelas regras de negócio, mas isso muitas vezes não se reflete na arquitetura de governança cloud. De forma prática, vemos isso:

![Varias aplicações na mesma conta](https://dev-to-uploads.s3.amazonaws.com/uploads/articles/e5iio892xnasr69imrx8.png)

Domínios distintos em uma única conta da AWS.

**Vantagem**: menos complexidade para fazer tudo.
**Desvantagem**: mais complexidade para governança

Num primeiro momento isso pode parecer eficiente, mas como as responsabilidades acabam se misturando, quando há necessidade de realizar alterações em alguns serviços, será difícil modificar uma dessas responsabilidades sem comprometer as outras.

Beleza, vamos começar a conversar sobre estratégias mais recomendadas para tratar a fundação da sua jornada cloud. Vamos segmentar contas de gerenciamento para outras contas.

![Segmentado contas](https://dev-to-uploads.s3.amazonaws.com/uploads/articles/xtwqauwqn9t1w3bgtmvx.png)

Como boa prática use a conta de gerenciamento apenas para gerenciamento da organização então vamos criar uma conta que será nossa conta de gerenciamento.

```hcl
#main.tf
resource "aws_organizations_account" "account" {
  name      = "my_new_account"
  email     = "tech@organizations.org"
}
```

Um motivo importante para manter seus recursos em outras contas é porque as políticas de controle de serviço (SCPs) do Organizations não funcionam para restringir nenhum usuário ou função na conta de gerenciamento.

Separar seus recursos de sua conta de gerenciamento também ajuda você a entender as cobranças em suas faturas.

Use um endereço de e-mail que encaminhe as mensagens recebidas diretamente para uma lista de gerentes de negócios sênior. No caso de a AWS precisar entrar em contato com o proprietário da conta, por exemplo, para confirmar o acesso, a mensagem de e-mail é distribuída para várias partes. Essa abordagem ajuda a reduzir o risco de atrasos nas respostas, mesmo se os indivíduos estiverem de férias, doentes ou abandonando o negócio.

Beleza, mas normalmente você vai precisar incluir contas em uma organização depois de criar (principalmente a conta antiga com tudo).

Agora vamos importar as contas antigas para debaixo da conta de gerenciamento usando import do terraforma e informando a account_id da conta antiga.

```hcl
$ terraform import aws_organizations_account.my_org 111111111111
```

Você também pode criar as novas contas já debaixo da nova estrutura usando o argumento parent_id no momento da criação da conta passando o ID da conta de gerenciamento.

```hcl
#main.tf
resource "aws_organizations_account" "account" {
  name      = "my_new_account"
  email     = "tech@organizations.org"
  parent_id = `111111111111`
} 
```

E isso, no próximo post vamos evoluir um pouco a estratégia de contas e agrupamentos.

\#vlwflw